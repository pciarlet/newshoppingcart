import React from 'react';
import Cards from './Cards';
import { Link } from 'react-router-dom';
import '../css/Cards.css';

const Home = () => {
    return (
        <div>
            <div className="title">
                <h1>STORE LOGO</h1>
            </div>
            <div className="upper__row">
                <h2>LAST PRODUCTS AVAILABLE!</h2>
                <p># products available{/* dynamic */}</p>
            </div>
            <div className="card__container">
                <Cards />
            </div>
            <div className="lower__row">
                <p># of prods added</p>{/*dynamic*/}
                <Link to="/cart">
                    <button>GO TO CART</button>
                </Link>
            </div>
        </div>
    )
}

export default Home;
