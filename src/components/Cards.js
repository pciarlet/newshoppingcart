import React from 'react';

import { Card } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import "../css/Cards.css";
import { useSelector, useDispatch } from 'react-redux';
import { increment } from '../actions';

const Cards = () => {

    const counter = useSelector(state => state.counter);
    const dispatch = useDispatch();


    return (
        <div className="carddiv">

            <Card className="card">
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(increment())}>Add to Cart</Button>
                </Card.Body>
            </Card>
            <Card className="card">
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(increment())}>Add to Cart</Button>
                </Card.Body>
            </Card>
            <Card className="card">
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(increment())}>Add to Cart</Button>
                </Card.Body>
            </Card>
            <Card className="card">
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(increment())}>Add to Cart</Button>
                </Card.Body>
            </Card>
            <Card className="card">
                <Card.Img variant="top" src="holder.js/100px180" />
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk of
                        the card's content.
                    </Card.Text>
                    <Button variant="primary" onClick={() => dispatch(increment())}>Add to Cart</Button>
                </Card.Body>
            </Card>


        </div>
    )
}

export default Cards
