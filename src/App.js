import React from 'react';
import Home from './components/Home';
import Cart from './components/Cart';
import './css/App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";



const App = () => {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/cart" component={Cart} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
